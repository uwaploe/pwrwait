package main

import "testing"

func TestPowerCheck(t *testing.T) {
	table := []struct {
		state, result bool
		systems       []string
		power         map[string]bool
	}{
		{
			state:   true,
			systems: []string{"A", "B", "C"},
			power:   map[string]bool{"A": true},
			result:  false,
		},
		{
			state:   false,
			systems: []string{"A", "B", "C"},
			power:   map[string]bool{"A": false, "B": false},
			result:  false,
		},
		{
			state:   true,
			systems: []string{"A", "B"},
			power:   map[string]bool{"A": true, "B": true},
			result:  true,
		},
		{
			state:   false,
			systems: []string{"A", "B"},
			power:   map[string]bool{"A": false, "B": false},
			result:  true,
		},
	}

	for _, e := range table {
		result := checkPower(e.state, e.systems, e.power)
		if result != e.result {
			t.Errorf("Bad result; got %v, expected %v (%v)",
				result, e.result, e)
		}
	}

}
