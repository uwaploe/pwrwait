module bitbucket.org/uwaploe/pwrwait

go 1.13

require (
	bitbucket.org/uwaploe/pducom v0.9.3
	github.com/garyburd/redigo v1.6.0
	github.com/gomodule/redigo v2.0.0+incompatible
)
