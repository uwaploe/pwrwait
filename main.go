// Pwrwait waits for one or more inVADER subsystems to be powered on or off.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/pducom"
	"github.com/gomodule/redigo/redis"
)

const Usage = `Usage: pwrwait [options] name [name ...]

Wait for one or more inVADER subsystems to be powered on or off (see --off
option). Exit status will be zero if the check is successful or 1 if the
check fails either due to a time-out or an invalid subsystem name.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	pwrOff = flag.Bool("off", false,
		"If true, wait for power-off rather than on")
	rdAddr   string        = "localhost:6379"
	msgQueue int           = 10
	timeOut  time.Duration = time.Second * 10
)

// Power status data records
type dataRecord struct {
	T    time.Time    `json:"time"`
	Src  string       `json:"source"`
	Data pducom.Power `json:"data"`
}

// Pass all received Redis pub-sub messages to a Go channel.
func pubsubReader(psc redis.PubSubConn, qlen int) <-chan redis.Message {
	c := make(chan redis.Message, qlen)
	go func() {
		defer close(c)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				select {
				case c <- msg:
				default:
					log.Printf("Queue full, %q message dropped", msg.Data)
				}
			}
		}
	}()

	return c
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.IntVar(&msgQueue, "qlen",
		lookupEnvOrInt("REDIS_QLEN", msgQueue),
		"message queue length")
	flag.StringVar(&rdAddr, "rdaddr",
		lookupEnvOrString("PDU_REDIS", rdAddr),
		"Redis host:port for message publishing")
	flag.DurationVar(&timeOut, "timeout",
		lookupEnvOrDuration("POWER_TIMEOUT", timeOut),
		"How long to wait for power on/off")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func checkPower(state bool, systems []string, power pducom.Power) bool {
	allmatch := true
	for _, sys := range systems {
		val, ok := power[sys]
		if !ok {
			log.Printf("Invalid subsystem name: %q", sys)
			return false
		}
		allmatch = allmatch && (val == state)
	}
	return allmatch
}

func main() {
	status := 0
	defer func() { os.Exit(status) }()

	args := parseCmdLine()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	// Redis connection for the pub-sub subscription
	rdconn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalf("Cannot connect to Redis: %v", err)
	}
	defer rdconn.Close()
	psc := redis.PubSubConn{Conn: rdconn}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	var tc <-chan time.Time
	if timeOut != 0 {
		tc = time.After(timeOut)
	}

	ch := pubsubReader(psc, msgQueue)
	psc.Subscribe("data.pdu.POWER")

	for {
		select {
		case <-tc:
			status = 1
			return
		case msg, ok := <-ch:
			if !ok {
				return
			}
			rec := dataRecord{}
			err = json.Unmarshal(msg.Data, &rec)
			if err == nil {
				if checkPower(!(*pwrOff), args, rec.Data) {
					status = 0
					return
				}
			}
		}
	}
}
